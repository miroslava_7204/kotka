import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.imageio.ImageIO;

public class GameFrame extends Frame implements Serializable{
	private static final long serialVersionUID = 1L;

	GamePanelToolbox panelToolbox;
	GamePanelMap panelMap;
	
	GameFrame(Karta k) {
		GameWindowListener gwl = new GameWindowListener(this);
		
		addWindowListener(gwl);
		this.setLayout(null);
		
		panelToolbox = new GamePanelToolbox();
		panelToolbox.setBounds(460, 30, 700, 500);
		panelToolbox.setLayout(null);
		this.add(panelToolbox);
		
		panelMap = new GamePanelMap(k);
		panelMap.setBounds(30, 30, 401, 251);
		panelMap.setLayout(null);
		this.add(panelMap);
	}
	
	
	
}
