import java.util.Scanner;

public class Kotka {
	private String ime;
	private String okraska;
	private String ka4estva;
	private int godini;
	private int kilogrami;
	

	public Kotka(String _ime, String _okraska, String _ka4estva, int _godini, int _kilogrami)
	{
	    ime = _ime;
	    okraska = _okraska;
	    ka4estva = _ka4estva;

	    if(_godini < 0)
	    {
            System.out.println("Годините не може да са отрицателни, правим ги 0");
            godini = 0;
        }
        else if(_godini > 20)
        {
            System.out.println("Годините не може да са повече от 20, правим ги 20");
            godini = 20;
        }
        else
        {
            godini = _godini;
        }

        if(_kilogrami < 0)
        {
            System.out.println("Килограмите не може да са отрицателни, правим ги 0");
            kilogrami = 0;
        }
        else if(_kilogrami > 30)
        {
            System.out.println("Килограмите не може да са повече от 30, правим ги 20");
            kilogrami = 30;
        }
        else
        {
            kilogrami = _kilogrami;
        }
	}

	String getIme()
	{
	    return ime;
	}

	String getOkraska()
	{
	    return okraska;
	}

	String getKa4estva()
	{
	    return ka4estva;
	}

	int getGodini()
	{
	    return godini;
	}

	int getKilogrami()
	{
        return kilogrami;
	}

    void setIme(String _ime)
	{
        ime = _ime;
	}

	void setOkraska(String _okraska)
	{
        okraska = _okraska;
	}

	void setKa4estva(String _ka4estva)
	{
        ka4estva = _ka4estva;
	}

	void setGodini(int _godini)
	{
	    if(_godini < 0)
	    {
            System.out.println("Годините не може да са отрицателни, правим ги 0");
            godini = 0;
        }
        else if(_godini > 20)
        {
            System.out.println("Годините не може да са повече от 20, правим ги 20");
            godini = 20;
        }
        else
        {
            godini = _godini;
        }
	}

	void setKilogrami(int _kilogrami)
	{
        if(_kilogrami < 0)
        {
            System.out.println("Килограмите не може да са отрицателни, правим ги 0");
            kilogrami = 0;
        }
        else if(_kilogrami > 30)
        {
            System.out.println("Килограмите не може да са повече от 30, правим ги 20");
            kilogrami = 30;
        }
        else
        {
            kilogrami = _kilogrami;
        }
	}

	void miay()
	{
	    System.out.println("Мяу!");
	}

	void razgonen()
	{
	    System.out.println("В търсене на женски!");
	}

	void biach()
	{
	    System.out.println("Are ela mi 1v1 we, le6nik!!1!");
	}

	public void iskam_hrana()
	{
	    System.out.println("Дай ми бисквити с кисело мляко, робе!" );

	    System.out.println("Mersi, sega sym " + (kilogrami+1) + " kilograma chist borsuk");
	}

	public void print()
	{
	    System.out.println("име: " + getIme());
        System.out.println("козина: " + getOkraska());
        System.out.println("качества: " + getKa4estva());
        System.out.println("години: " + getGodini());
        System.out.println("килограми: " + getKilogrami());
	}
	
	Scanner scan1 = new Scanner(System.in);
    public void nahrani()
    {
        System.out.println("да се нахрани ли котката?");
        String g = "";
        g = scan1.nextLine();
        if (g.equals("да")) {
            iskam_hrana();    
        }
        else if(g.equals("не")) { 
            miay(); 
        }
    }
}


