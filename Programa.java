import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Programa {
	public static void main(String[] args) {

		Karta k;
		Hero h;
		
		try {
			FileInputStream fis = new FileInputStream("save.bin");
			ObjectInputStream ois = new ObjectInputStream(fis);
			k = (Karta)ois.readObject();
			ois.close();
			fis.close();
		} catch (IOException | ClassNotFoundException e) {
			h = new Hero(1, 2);
			k = new Karta(9, 6, h);
			
		}
		GameFrame f = new GameFrame(k);
		f.setSize(f.getMaximumSize());
		f.setVisible(true);
		
	}
}
