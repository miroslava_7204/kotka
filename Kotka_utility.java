import java.util.Random;

public class Kotka_utility {
	private Kotka_utility () {}

    public static final Kotka sbivat_se (Kotka a, Kotka b)
    {
        Random rand = new Random();
        int koef_a = 20*a.getGodini() + 13*a.getKilogrami() + rand.nextInt(3);
        int koef_b = 20*b.getGodini() + 13*b.getKilogrami() + rand.nextInt(3);
        if(koef_a < koef_b)
            return b; // b shte jivee
        else if(koef_a > koef_b)
            return a; // a shte jivee
        else
            return null; // i dvete 6te umrat
    }

    public static final Kotka razmnojavat_se (Kotka a, Kotka b, String ime_na_deteto)
    {
        Random rand = new Random();

        String okraska_child;
        if(rand.nextInt(2)==0)
            okraska_child = a.getOkraska();
        else
            okraska_child = b.getOkraska();

        String ka4estva_child;
        if(rand.nextInt(2)==0)
            ka4estva_child = a.getKa4estva();
        else
            ka4estva_child = b.getKa4estva();

        return new Kotka(ime_na_deteto, okraska_child, ka4estva_child, 0, 1);
    }
}

