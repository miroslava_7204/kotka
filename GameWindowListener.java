import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class GameWindowListener implements WindowListener, Serializable {
	GameFrame frame;
	Hero hero;
	GameWindowListener(GameFrame f){
		frame = f;
	}

	@Override
	public void windowOpened(WindowEvent e)  {
	}

	@Override
	public void windowClosing(WindowEvent e) {
		
		try {
		
			FileOutputStream fos = new FileOutputStream("save.bin");
			
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(frame.panelMap.karta);
			oos.close();
			fos.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.exit(0);
	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
}

