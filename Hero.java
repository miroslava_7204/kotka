import java.io.Serializable;

public class Hero implements Serializable {
		int posx;
		int posy;
		
		Hero(int x, int y) {
			posx = x;
			posy = y;
		}
		
		void move(char c) {
            int old_posx = posx;
            int old_posy = posy;
			switch(c) {
				case 'w': posy--; break;
				case 's': posy++; break;
				case 'a': posx--; break;
				case 'd': posx++; break;
			}
	        if(posx < 0)
	            posx = 0;
	        if(posy < 0)
	            posy = 0;
	        if(posx >= 7)
	            posx = 7;
	        if(posy >= 4)
	            posy = 4;
            if(posx == 7 && posy == 3)
            {
                posx = old_posx;
                posy = old_posy;
            }
         
            if(posx == 3 && posy == 1) { 
                try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
                System.exit(0);
            }
      
	  }
}