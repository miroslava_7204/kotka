import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.imageio.ImageIO;

public class GamePanelMap extends Panel implements Serializable{
	Karta karta;
	
	GamePanelMap(Karta k) {
		karta = k;
		
		GameKeyListener gkl = new GameKeyListener(this);
		addKeyListener(gkl);
		
		GameMouseListener gml = new GameMouseListener(this);
		addMouseListener(gml);
	}
	
	public void paint(Graphics g) {
		BufferedImage img = null;
		BufferedImage katq_img = null;
		BufferedImage house_img = null;
		File f = new File("hero.png");
		File f_katq = new File("katq.png");
		File f_house = new File("house.png");
		try {
			img = ImageIO.read(f);
			katq_img = ImageIO.read(f_katq);
			house_img = ImageIO.read(f_house);
		} catch (IOException e) {
			e.printStackTrace();
		}
		g.drawImage(img, karta.hero.posx * 50 , karta.hero.posy * 50, null);
		g.drawImage(katq_img, 5 * 50 , 1 * 50, null);
		g.drawImage(house_img, 3 * 50 , 1 * 50, null);
        Color prevColor = g.getColor();
        Color limeGreen = new Color(224, 231, 218);
        for (int i = 0; i < karta.sizex; i++) {
			for (int j = 0; j < karta.sizey; j++) {
				g.drawRect(i * 50 , j * 50,  50,  50);
				g.setColor(limeGreen);
				g.fillRect(i * 50 + 1 , j * 50 + 1, 50 - 2, 50 - 2);
				g.setColor(prevColor);
			}
		}
        g.drawImage(img, karta.hero.posx * 50 , karta.hero.posy * 50, null);
        g.drawRect(karta.hero.posx * 50 , karta.hero.posy * 50,  50,  50);
		g.drawImage(katq_img, 5 * 50 , 1 * 50, null);
		g.drawRect(5 * 50 , 1 * 50,  50,  50);
		g.drawImage(house_img, 3 * 50 , 1 * 50, null);
		g.drawRect(3 * 50 , 1 * 50,  50,  50);
		
//		try {
//			if (karta.hero.posx == 3 )
//			{ 
//			    Thread.sleep(5000);
//			    System.exit(0);
//			}
//		} catch (InterruptedException e1) {
//			e1.printStackTrace();
//		}
	}
}
