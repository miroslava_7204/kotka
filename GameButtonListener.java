import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

public class GameButtonListener implements ActionListener {
	GamePanelToolbox panel;
	
	GameButtonListener(GamePanelToolbox p) {
		panel = p;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		String s = panel.textField.getText();
		if(s.isEmpty()) 
		{
		    panel.label.setText("котката е жива");
		}
		else
		{
		    panel.label.setText(s);
		}
		
		panel.textField.setVisible(false);
		panel.textArea.setVisible(true);
		
		
	}
}